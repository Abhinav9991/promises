const fs = require("fs")
const path = require("path")

const problem1 = (newDirectory,count) => {
    const path = __dirname + "/" + newDirectory
    const makeDirectory = (path) => {
        return new Promise((resolve, reject) => {
            fs.mkdir(path, (err) => {
                if (err) {
                    reject(err)
                }
                else {
                    resolve("action completed successfully")
                }
            })
        })
    }

    const makeFiles = (path, count) => {
        return new Promise((resolve, reject) => {
            let file = 1
            const id = setInterval(() => {
                if (count > file) {
                    let filename = path + `/randomfile${file}.json`
                    fs.writeFile(filename, `${file} is created`, (err) => {
                        if (err) {
                            reject(err)
                        }
                        else {
                            resolve("files created")
                        }
                    })
                }else{
                    clearInterval(id)
                }
                file++
            }, 1000)

        })
    }

    
    const deleteFile = (path) =>setTimeout(() => {
        
        fs.readdir(path, (err, data) => {
            if (err) {
                console.log(err);
            }
            let deleteFiles = data.map((file) => {
                let deletePath = path + '/' + file
                fs.unlink(deletePath, (err) => {
                    if (err) {
                        console.log(err)
                    }
                    else {
                        console.log("File deleted")
                    }
                })
                return true
            })
        })


    }, 8000);
    makeDirectory(path)
        .then((result) => {
            console.log(result);
            return makeFiles(path,count)
        })
        .then((result) => {
            console.log(result);
            return deleteFile(path)
            
        })
        .catch((result) => {
            console.log(result);
        })

}


module.exports = problem1
// let i = 0;

// const id = setInterval(() => {
//     let file = 0;
//     console.log(i)
//     i++;
//     if(i > 5) {
//         clearInterval(id);
//     }
// }, 1000);

// setTimeout(() => {
    // console.log("Over");
// }, 7000);

