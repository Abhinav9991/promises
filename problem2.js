const fs = require("fs");
const path = __dirname + "/data/"

const problem2 = () => {
    let nameFile = "FileNames.txt";
    const readFile = () => {
        return new Promise((resolve, reject) => {
            let lipsumPath = path + 'lipsum.txt';
            fs.readFile(lipsumPath, 'utf-8', (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(data);
                }
            });

        });

    }
    const LipsumUpperText = (data) => {
        return new Promise((resolve, reject) => {
            let upperCaseData = data.toUpperCase();
            let FileName = "upperCaseFile.txt";
            let FilePath = path + FileName;
            fs.writeFile(FilePath, upperCaseData, (err) => {
                if (err) {
                    reject(err);
                } else {
                    storeFileName(FileName)
                    resolve(FilePath);
                }
            });
        });


    }

    const LipsumLowerText = (filePath) => {

        return new Promise((resolve, reject) => {

            fs.readFile(filePath, 'utf-8', (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    let lowerCaseFile = data.toLowerCase()
                        .split(' ');
                    let joinedLowerCaseText = lowerCaseFile.join('\n');
                    let lowerCaseName = "splitToLowerCaseFile.txt";
                    let lowerCasePath = path + lowerCaseName;
                    fs.writeFile(lowerCasePath, joinedLowerCaseText, (err) => {
                        if (err) {
                            reject(err);
                        } else {
                            storeFileName(lowerCaseName);
                            resolve(lowerCasePath);
                        }
                    });
                }
            });
        });

    }
    const sortedtext = (filePath) => {
        return new Promise((resolve, reject) => {
            fs.readFile(filePath, 'utf-8', (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    let sortedFile = data.split('\n')
                        .sort()
                        .join(' ');
                    let sortedName = "sortedFile.txt";
                    let sortedFilePath = path + sortedName;
                    fs.writeFile(sortedFilePath, sortedFile, (err) => {
                        if (err) {
                            reject(err);
                        } else {
                            
                            resolve(sortedName);
                        }
                    });
                }
            });
        });
    }

    const  deleteFiles=()=>{
        let nameFilePath = path + nameFile;
        fs.readFile(nameFilePath, 'utf-8', (err, data) => {
            if (err) {
                console.log(err);
            } else {
                let filesToBeDeleted = data.split(' ');
                let filesDeleted = filesToBeDeleted.map((file) => {
                    let deleteFilePath = path + file;
                    fs.unlink(deleteFilePath, (err) => {
                        if (err) {
                            console.log(err);
                        } else {
                            console.log(`${file} deleted successfully`);
                        }
                    })
                });
            }
        });
    }





    const storeFileName = (fileName) => {
        let filePath = path + nameFile;
        fs.appendFile(filePath, (fileName + " "), function (err) {
            if (err) {
                console.log(err);
            } else {
                console.log(`Updated file names with ${fileName}`);
            }
        });
    }



    readFile()
        .then((result) => {
            console.log(result);
            return LipsumUpperText(result)

        })
        .then((uppertextFilePath) => {
            return LipsumLowerText(uppertextFilePath)
        })
        .then((lowerCasePath) => {
            return sortedtext(lowerCasePath);
        })
        .then((sortedName)=>{
            storeFileName(sortedName)
            setTimeout(() => {
                deleteFiles()
            }, 1000);
        })
        .catch((result) => {
            console.log(result);
        })


}

module.exports = problem2
